from utils import get_data
from utils import get_date
from utils import get_json
from datetime import timedelta
import re


class FileFunctions:

    def _set_defaults(self, file_constants):
        self.header = file_constants.get('header')
        self.hour_prefix = file_constants.get('hour_prefix')
        self.del_location_key = file_constants.get('del_location_key')
        self.del_type_key = file_constants.get('del_type_key')
        self.value_type_key = file_constants.get('value_type_key')
        include_del_location_regex = file_constants.get('include_del_location_regex')
        self.include_del_location = re.compile(include_del_location_regex)

    def column_prices(self, response, file_constants, insert_function, value_function):
        self._set_defaults(file_constants)
        file_lines = response.iter_lines()
        hour_column_key = file_constants.get('hour_column_key')
        table_name = file_constants.get('table_name')
        include_columns = file_constants.get('include_columns')
        date = get_date(file_lines)
        data = get_data(file_lines, self.header)
        values = []
        for row in data:
            for column_key in row:
                if column_key in include_columns:
                    hours = row[hour_column_key].split(self.hour_prefix)[1]
                    hour_beginning = date + timedelta(hours=hours)
                    price = row[column_key]
                    del_location = self.include_del_location.match(column_key).group()[0]
                    values.append((del_location, hour_beginning, price))
        insert_function(values, table_name, file_constants)

    def get_json(self, response, file_constants, insert_function, value_function):
        data = get_json(response)
        value_function(data, file_constants, insert_function)



    def transposed(self, response, file_constants, insert_function, value_function):

        file_lines = response.iter_lines()
        header = file_constants.get('header')
        hour_prefix = file_constants.get('hour_prefix')
        del_location_key = file_constants.get('del_location_key')
        del_type_key = file_constants.get('del_type_key')
        value_type_key = file_constants.get('value_type_key')
        include_value = file_constants.get('include_value')
        ignore_del_types = file_constants.get('ignore_del_types')
        table_name = file_constants.get('table_name')
        include_del_location_regex = file_constants.get('include_del_location_regex')
        include_del_location = re.compile(include_del_location_regex)

        date = get_date(file_lines)
        data = get_data(file_lines, header)
        values = []
        for line in data:
            hours = filter(lambda item: item.find(hour_prefix) > -1, line)
            del_location = line[del_location_key]
            del_type = line[del_type_key]
            del_location_groups = include_del_location.match(del_location)
            value_type = line.get(value_type_key)
            if del_location_groups is not None and del_type not in ignore_del_types and include_value == value_type:
                for hour in hours:
                    price = line[hour]
                    del_location_name = del_location_groups.group(1)
                    hour = int(hour.split(hour_prefix)[1])
                    hour_beginning = date + timedelta(hours=hour)
                    values.append((del_location_name, hour_beginning, price))
        insert_function(values, table_name)

