from utils import get_unzipped_stream
from utils import xls_csv
from json

class ResponseFunctions:

    def get_file(self, response, file_function, file_consts, insert_data_function, value_function):
        file_function(response, file_consts, insert_data_function, value_function)

    def get_zip(self, response, file_function, file_constants, insert_data_function):
        unzipped_stream = get_unzipped_stream(response)
        for file_name in unzipped_stream.filelist:
            lines = unzipped_stream.open(file_name).readlines()
            file_function(lines, file_constants, insert_data_function)

    def get_xls_zip(self, response, file_function, file_constants, insert_data_function):
        unzipped_stream = get_unzipped_stream(response)
        for file_name in unzipped_stream.filelist:
            lines = xls_csv(unzipped_stream.open(file_name))
            file_function(lines, file_constants, insert_data_function)
