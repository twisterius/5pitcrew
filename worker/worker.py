import requests

from response_functions import ResponseFunctions
from file_functions import FileFunctions
from value_functions import ValueFunctions
import requests
import MySQLdb
import yaml
from datetime import date
from datetime import datetime
from datetime import timedelta
import sys
import logging
import traceback
import Queue
import threading
from utils import chunk
import os
from dateutil.relativedelta import relativedelta


DEFAULT_CHUNK_SIZE = 8192

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

class Worker:

    def __init__(self, iso):
        """
        initialize db,
        set response functions object,
        set file functions objcet
        :param iso: str, the iso name
        """
        with open('/home/ubuntu/workers/worker/db.yaml', 'r') as stream:
            db_creds = yaml.load(stream)

        self.iso = iso
        self.response_funcs = ResponseFunctions()
        self.file_funcs = FileFunctions()
        self.value_funcs = ValueFunctions()
        self.db = MySQLdb.connect(host=db_creds['host'], user=db_creds['user'], passwd=db_creds['password'], db=db_creds['db_name'])
        self.cursor = self.db.cursor()
        self.q = Queue.Queue()

    def _get_url(self, day=None, date_fmt='', prefix='', postfix=''):
        """
        returns url for date in specified format

        @day datetime.date | None, the date we are capturing the url for
        @date_fmt: str, the format of the date for the url we are composing
        @prefix: str, the prefix url
        @postfix: str, the postfix of the url
        @returns: str, the formatted url
        """
        if day is not None:
            date_string = day.strftime(date_fmt)
        else:
            date_string = ''
        url = '{0}{1}{2}'.format(prefix, date_string, postfix)
        return url

    def _get_request(self, url):
        """
        returns the req response
        :param url: str, the url to hit
        :return: resp obj, the response
        """
        return requests.get(url)

    def _process_response(self, response, response_function, file_function, value_function, file_consts):
        '''
        processes a response via the process function, then iterates through the file results
        dispatching the file function

        :param response: obj, response object
        :param response_function: fun, function use to process response and return result files as iterable
        :param file_consts: dict, params for file function
        :param file_function: fun, function use to process response and return result files as iterable
        '''
        #todo remove check after price value function refactor
        if value_function is not None:
            value_function = getattr(self.value_funcs, value_function)
        getattr(self.response_funcs, response_function)(response,
                                                        getattr(self.file_funcs, file_function),
                                                        file_consts, self._insert_data,
                                                        value_function)

    def _chunk_values(self, values):
        """ Yield successive n-sized chunks from l.
        """
        n = self.chunk_size
        for i in xrange(0, len(values), n):
            yield values[i:i+n]

    def _insert_data(self, values, table_name, file_constants):
      """
      Performs bulk insert for data

      :param values: values to be inserted
      :param table_name: the table name
      """
      columns = file_constants.get('columns')
      logging.info("inserting {0} values for date: {1}".format(len(values), values[0][1]))
      for values_chunk in chunk(values, DEFAULT_CHUNK_SIZE):
            insert_tuple = '({0})'.format(','.join(['?' for column in columns]))
            query = '''INSERT IGNORE INTO {table_name} {columns} VALUES {insert_tuple}'''.format(table_name=table_name,
                                                                                                 columns=columns,
                                                                                                 insert_tuple=insert_tuple)
            self.cursor.executemany(query, values_chunk)
            self.db.commit()

    def _get_data_for_date(self, day=None, url_fmt=None,
                           response_function=None,
                           file_function=None, file_constants=None,
                           value_function=None):
        """
        gets, processes and inserts data for date

        :param day: datetime.date | None, date specified
        :param url_fmt: dict, the dict descriptions of the url format
        :param response_function: fun, function use to process response and return result files as iterable
        :param file_function: fun, function use to process response and return result files as iterable
        """
        url = self._get_url(day, **url_fmt)
        response = self._get_request(url)

        self._process_response(response, response_function, file_function, value_function, file_constants)

    def _attempt_strategies_for_date(self, day, strategies):
        """
        iterates through the isos strategies and hopefully finds the solution

        :param day: datetime.datetime, date to get the prices for

        """
        for strategy in strategies:
            strategy = strategy.get('strategy')
            url_fmt = strategy.get('url_fmt')
            response_function = strategy.get('response_function')
            file_function = strategy.get('file_function')
            value_function = strategy.get('value_function')
            file_constants = strategy.get('file_constants')
            try:
                self.q.put(self._get_data_for_date(day=day, url_fmt=url_fmt, response_function=response_function,
                                       file_function=file_function, file_constants=file_constants, value_function=value_function)
                break
            except Exception as e:
                logging.warn("traceback: {0}".format(traceback.format_exc()))

    def _within_time_range(self, strategy, date_time):
        default_start = {'year': 1990, 'month': 1, 'day': 1}
        default_end = {'year': 2099, 'month': 1, 'day': 1}
        start = datetime(**strategy['date_range'].get('start', default_start))
        end = datetime(**strategy['date_range'].get('end', default_end))
        return start < date_time < end

    def _run_strategies(self, date_time, path):
        strategies = []
        for file_name in os.listdir(path):
                with open('{0}/{1}'.format(path, file_name)) as stream:
                    strategy = yaml.load(stream)
                    strategies.append(strategy)
        filtered_strategies = filter(lambda strat: self._within_time_range(strat, date_time), strategies)
	self._attempt_strategies_for_date(date_time, filtered_strategies)
        #t = threading.Thread(target=self._attempt_strategies_for_date, args=(date_time,filtered_strategies))
        #t.daemon = True
        #t.start()

        #self._attempt_strategies_for_date(date_time, filtered_strategies)

    def _find_strategies(self, date_time, iso):
        if iso.find('/') == -1:
            for dir in os.listdir('isos/{0}/'.format(iso)):
                path = 'isos/{0}/{1}'.format(iso, dir)
                self._run_strategies(date_time, path)
        else:
            path = 'isos/{0}/'.format(iso)
            self._run_strategies(date_time, path)




    def get_data_since_date(self, date_time, increment=None, until=None):
        if until is None:
            until = date.today()
        hour_0 = datetime(until.year, until.month, until.day, 0)
        earliest_day = datetime(date_time.year, date_time.month, date_time.day, 0)
        current_day = hour_0 + timedelta(days=2)
        increment
        while earliest_day < current_day:
            self._find_strategies(current_day, self.iso)
            if increment is None:
                current_day = current_day - relativedelta(days=1)
            else:
                delta = {increment: 1}
                current_day = current_day - relativedelta(**delta)
        self.q.get()

def main():
    iso = sys.argv[1]
    if len(sys.argv) < 3:
        today = date.today()
        since = today - timedelta(days = 5)
        year = since.year
        month = since.month
        day = since. day
        increment = 'days'
        until_year = today.year
        until_day = today.day
        until_month = today.month
    else:      
        year = int(sys.argv[2])
        month = int(sys.argv[3])
        day = int(sys.argv[4])
        increment = sys.argv[5]
        until_year = int(sys.argv[6])
        until_month =int(sys.argv[7])
        until_day =int(sys.argv[8])
    worker = Worker(iso)
    date_time = datetime(year, month, day)
    until = datetime(until_year, until_month, until_day)
    worker.get_data_since_date(date_time, increment, until)

main()
