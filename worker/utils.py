import csv
import re
from dateutil import parser
import StringIO
import zipfile
import pandas as pd
from json import loads

DATE_MATCH = re.compile(r'[0-9]{1,2}[-/][0-9]{1,2}[-/][0-9]{2,4}')

def xls_csv(f):
    data_xls = pd.read_excel(f, 'Sheet1', index_col=None)
    return data_xls.to_string().splitlines()

def get_date(lines):
    for line in lines:
        match = DATE_MATCH.match(line)
        if match is not None:
            return parser.parse(match.group())

def get_json(response):
    return loads(response.text)

def get_data(lines, header):
    indexes = [i for i, line in enumerate(lines) if line.startswith(header)]
    header_index = indexes[0]
    data = lines[header_index:]
    return csv.DictReader(data)

def chunk(values, n):
        """ Yield successive n-sized chunks from l.
        """
        for i in xrange(0, len(values), n):
            yield values[i:i+n]

def get_unzipped_stream(response):
        stream = StringIO.StringIO(response.content)
        return zipfile.ZipFile(stream)